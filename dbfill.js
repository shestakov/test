var mysql = require('mysql');

var con = mysql.createConnection({
	host: "localhost",
	user: "test",
	password: "test",
	database: "test"
});

con.connect();


con.query(`DROP TABLE IF EXISTS author CASCADE ;`, function (err, result, fields) {
	if (err) throw err; console.log(result);});

con.query(`DROP TABLE IF EXISTS book CASCADE;`, function (err, result, fields) {
	if (err) throw err; console.log(result);});



con.query(`
	CREATE TABLE IF NOT EXISTS author (
		id INT NOT NULL AUTO_INCREMENT,
		name varchar(255),
		birthday date,
		PRIMARY KEY (ID)
	);`,
				
	function (err, result, fields) {
		if (err) throw err;
		console.log(result);
	}
);

con.query(`
	CREATE TABLE IF NOT EXISTS book (
		title varchar(255),
		date date,
		author INT,
		description varchar(255),
		image varchar(255),
		
		FOREIGN KEY (author)
			REFERENCES author(id)
			ON DELETE CASCADE
	);`,
				
	function (err, result, fields) {
		if (err) throw err;
		console.log(result);
	}
);


function getRndString() {
	return Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5);
}

function getRndInteger(min, max) {
	return Math.floor(Math.random() * (max - min) ) + min;
}

function getRndDate() {
	return `${getRndInteger(1800,2000)}-${getRndInteger(1,12)}-${getRndInteger(1,31)}`;
}

function insertAuthor() {
	con.query(`
		INSERT INTO author (name, birthday)
		VALUES ("${getRndString()}", "${getRndDate()}");
		`,
		function (err, result, fields) {
			if (err) throw err;
			//console.log(result);
		}
	);
}

for(var i=1; i<4; i++) {
	insertAuthor();
}


function insertBook() {
	con.query(`
		INSERT INTO book
		VALUES ("${getRndString()}", "${getRndDate()}", "${getRndInteger(1,3)}", "${getRndString()}", "${getRndString()}");
		`,
		function (err, result, fields) {
			if (err) throw err;
			//console.log(result);
		}
	);
}

for(var i=0; i<1e5; i++) {
	insertBook();
}


con.end()
