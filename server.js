const Koa = require('koa');
const Router = require('koa-router');

const redis = require('redis');
const mysql = require('mysql');

var config = {
	host: "localhost",
	user: "test",
	password: "test",
	database: "test"
};

class Database {
	constructor( config ) {
		this.connection = mysql.createConnection(config);
	}
	query(sql, args) {
		return new Promise((resolve, reject) => {
			this.connection.query(sql, args, (err, rows) => {
				if (err) return reject(err);
				resolve(rows);
			});
		});
	}
	close() {
		return new Promise( ( resolve, reject ) => {
			this.connection.end( err => {
				if (err) return reject( err );
				resolve();
			});
		});
	}
}

class Cache {
	constructor() {
		this.client = redis.createClient(process.env.REDIS_PORT);
	}
	getValue(key) {
		return new Promise((resolve, reject) => {
			this.client.get(key, (err, data) => {
				if (err) return reject(err);
				resolve(data);
			});
		});
	}
	setValue(key, value) {
		this.client.setex(key, 3600, value);
	}
}


const database = new Database(config);
const app = new Koa();
const router = new Router();
const cache = new Cache();





//client.get('some key', function (err, data) {
//console.log(data);
//});

router
	.get('/book', async (ctx, next) => {
		var v = await cache.getValue(ctx.originalUrl);
		if (v) {
			ctx.body = {
				status: 'success',
				result: JSON.parse(v),
			};
		
		} else {
	
			var q = `
				SELECT book.*, author.id as author_id
				FROM book
				LEFT JOIN author ON book.author = author.id
				${ctx.request.query.groupby ? 'GROUP BY ' + ctx.request.query.groupby : ''}
				ORDER BY ${ctx.request.query.orderby || 'title'} ${(ctx.request.query.order || 'ASC')}
				LIMIT ${ctx.request.query.offset || 0}, ${ctx.request.query.limit || 10 }
			;`;
			console.log(q);
			
			var r = await database.query(q);
			cache.setValue(ctx.originalUrl, JSON.stringify(r));
			ctx.body = {
				status: 'success',
				result: r,
			};
		
		}
		next();
	})
	.post('/book', async (ctx, next) => {
		var q = `
			INSERT INTO book
			VALUES (
				"${ctx.request.query.title}",
				"${ctx.request.query.date}",
				"${ctx.request.query.author}",
				"${ctx.request.query.description}",
				"${ctx.request.query.image}"
			)
		;`;
		console.log(q);
		
		var r = await database.query(q);
		ctx.body = {
			status: 'success',
			result: r,
		};
	})
	.put('/book/:id', async (ctx, next) => {
		var q = `
			UPDATE book
			SET
				title = "${ctx.request.query.title}",
				date = "${ctx.request.query.date}",
				author = ${ctx.request.query.author},
				description = "${ctx.request.query.description}",
				image = "${ctx.request.query.image}"
			WHERE id=${ctx.params.id}
			
		;`;
		
		var r = await database.query(q);
		ctx.body = {
			status: 'success',
			result: r,
		};
	})


app
	.use(async (ctx, next) => {
		console.log(ctx.method, ctx.originalUrl, new Date().toLocaleString());
		await next(ctx);
	})
	.use(router.routes())
	.use(router.allowedMethods());

app.listen(3000);
